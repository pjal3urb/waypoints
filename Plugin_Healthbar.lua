if RequiredScript == "lib/units/enemies/cop/copdamage" then

	local _on_damage_received_original = CopDamage._on_damage_received

	function CopDamage:_on_damage_received(...)
		managers.waypoints:do_callback("wp_healthbar_" .. tostring(self._unit:key()), "damage_received")
		return _on_damage_received_original(self, ...)
	end
	
end

if RequiredScript == "lib/units/equipment/sentry_gun/sentrygundamage" then

	local _apply_damage_original = SentryGunDamage._apply_damage
	local repair_shield_original = SentryGunDamage.repair_shield
	
	function SentryGunDamage:_apply_damage(...)
		local result = _apply_damage_original(self, ...)
		managers.waypoints:do_callback("wp_healthbar_" .. tostring(self._unit:key()), "damage_received")
		return result
	end
	
	function SentryGunDamage:repair_shield(...)
		local result = repair_shield_original(self, ...)
		managers.waypoints:do_callback("wp_healthbar_" .. tostring(self._unit:key()), "damage_received")
		return result
	end

end

if RequiredScript == "lib/units/beings/player/playermovement" then

	local update_original = PlayerMovement.update

	local tank_ids = {
		tank = true,
		tank_medic = true,
		tank_mini = true,
	}
	
	local function check_unit_wp(unit)
		if alive(unit) then
			local id = "wp_healthbar_" .. tostring(unit:key())
			local wp = managers.waypoints:get_waypoint(id)
			
			if not wp then
				wp = managers.waypoints:add_waypoint(
					id, 
					unit:in_slot(25) and TurretHealthbarWaypoint or tank_ids[unit:base()._tweak_table] and DozerHealthbarWaypoint or HealthbarWaypoint,
					{ unit = unit })
			end
			
			wp:refresh()
			return true
		end
	end
	
	local from = Vector3()
	local to = Vector3()
	local function simple_raycast()
		local player = managers.player:player_unit()
		
		if alive(player) then
			mvector3.set(from, player:camera():position())
			mvector3.set(to, player:camera():forward())
			mvector3.multiply(to, 20000)
			mvector3.add(to, from)
		
			local ray = World:raycast("ray",from, to, "slot_mask", managers.slot:get_mask("bullet_impact_targets"))
			
			if ray and ray.unit then
				if ray.unit:in_slot(12) or (ray.unit:in_slot(25) and ray.unit:base() and string.find(ray.unit:base():get_type(), "turret")) then
					return ray.unit
				end
			end
		end
	end
	
	function PlayerMovement:update(...)
		if self._current_state then
			if self._current_state._fwd_ray_new then
				for i, data in ipairs(self._current_state._fwd_ray_new:get_targets_by_type({ "enemy", "swat_turret" }, true)) do
					if check_unit_wp(data.unit) then
						break
					end
				end
			else
				check_unit_wp(simple_raycast())
			end
		end
		
		return update_original(self, ...)
	end
	
	
	if not CustomWaypoint then return end	--Suppress error in menu
	
	
	HealthbarWaypoint = HealthbarWaypoint or class(CustomWaypoint)
	HealthbarWaypoint.WIDTH = 100
	HealthbarWaypoint.BAR_HEIGHT = 10
	HealthbarWaypoint.BORDER_SIZE = 1
	HealthbarWaypoint.TTL_MAX = 3
	HealthbarWaypoint.Y_OFFSET = 30
	function HealthbarWaypoint:init(id, ws, data)
		HealthbarWaypoint.super.init(self, id, ws, data)
		
		self._w = self.WIDTH
		self._h = 0
		self._bar_width = self._w - self.BORDER_SIZE * 2
		self._bars = {}
		self._borders = {}
		self._unit = data.unit
		
		self._panel:set_w(self._w)
		
		self._bg = self._panel:rect({
			name = "bg",
			color = Color.black,
			alpha = 0.75,
			layer = -10,
			valign = "grow",
			halign = "grow",
		})
		
		self:_add_bar("health", Color.red)
	end
	
	function HealthbarWaypoint:post_init()
		for _, bar in pairs(self._bars) do
			bar:set_valign("scale")
		end
		
		self:damage_received()
	end
	
	function HealthbarWaypoint:_add_bar(name, color)
		local count = table.size(self._bars)
	
		self._bars[name] = self._panel:bitmap({
			name = name,
			texture = "guis/textures/menu_selected",
			texture_rect = { 0, 17, 256, 30 },
			color = color,
			alpha = 0.8,
			halign = "scale",
			w = self._bar_width,
			h = self.BAR_HEIGHT,
			y = count * self.BAR_HEIGHT,
			x = self.BORDER_SIZE,
		})
		
		self._h = self._h + self.BAR_HEIGHT
		self._panel:set_h(self._h)
		
		local borders = {
			left = { w = self.BORDER_SIZE, valign = "grow", align = "left" },
			right = { w = self.BORDER_SIZE, valign = "grow", align = "right", x = self._w - self.BORDER_SIZE },
			bottom = { h = self.BORDER_SIZE, halign = "grow", vertical = "bottom", y = self._h - self.BORDER_SIZE },
			top = (count == 0) and { h = self.BORDER_SIZE, halign = "grow", vertical = "top" } or nil,
		}
		
		for align, data in pairs(borders) do
			local border = self._panel:rect({
				color = Color.white,
				alpha = 0.75,
				layer = 5,
				h = data.h,
				w = data.w,
				align = data.align,
				vertical = data.vertical,
				valign = data.valign,
				halign = data.halign,
				y = data.y,
				x = data.x
			})
		end
		
	end

	function HealthbarWaypoint:_alive()
		return self._enabled and not self._dead and alive(self._unit) and self._ttl > 0
	end

	function HealthbarWaypoint:_update_world_position(t, dt)
		return self._unit:movement():m_head_pos() + math.UP * self.Y_OFFSET
	end

	function HealthbarWaypoint:_displace_screen_position()
		return 0, -10
	end

	function HealthbarWaypoint:_update(t, dt, distance, dot, angle, x, y)
		self._panel:set_layer(-math.round(distance/10))
		local angle_mod = math.lerp(1, 5, math.clamp((angle - 5)/25, 0, 1))
		local los_mod = 1	--TODO
		
		self._ttl = self._ttl - dt * angle_mod * los_mod
		if self._ttl < self.TTL_MAX / 3 then
			self._panel:set_alpha(3*self._ttl/self.TTL_MAX)
		end
	end
	
	function HealthbarWaypoint:damage_received()
		self._dead = self._unit:character_damage():dead()
		self._bars.health:set_w(self._bar_width * math.max(self._unit:character_damage():health_ratio(), 0))
		
	end

	function HealthbarWaypoint:refresh()
		self._ttl = self.TTL_MAX
		self._panel:set_alpha(1)
	end
	
	
	DozerHealthbarWaypoint = DozerHealthbarWaypoint or class(HealthbarWaypoint)
	DozerHealthbarWaypoint.Y_OFFSET = 40
	DozerHealthbarWaypoint.VISORS = {
		[4] = 15,	--PLATE VISOR (15)
		[5] = 16,	--GLASS VISOR (16)
	}
	DozerHealthbarWaypoint.VISOR_MAX = 0
	for _, value in pairs(DozerHealthbarWaypoint.VISORS) do
		DozerHealthbarWaypoint.VISOR_MAX = DozerHealthbarWaypoint.VISOR_MAX + value
	end
	
	function DozerHealthbarWaypoint:init(...)
		DozerHealthbarWaypoint.super.init(self, ...)
		self:_add_bar("visor", Color(0.6, 0.6, 0.6))
	end
	
	function DozerHealthbarWaypoint:damage_received()
		DozerHealthbarWaypoint.super.damage_received(self)
		
		if not self._visor_destroyed then
			local visor_health = 0
			
			for i, value in pairs(DozerHealthbarWaypoint.VISORS) do
				local damage = self._unit:body(i):extension().damage._damage.damage or 0
				
				if damage < value then
					visor_health = visor_health + value - damage
				end
			end
			
			self._bars.visor:set_w(self._bar_width * math.clamp(visor_health / DozerHealthbarWaypoint.VISOR_MAX, 0, 1))
			
			if visor_health <= 0 then
				self._visor_destroyed = true
			end
		end
	end


	TurretHealthbarWaypoint = TurretHealthbarWaypoint or class(HealthbarWaypoint)
	TurretHealthbarWaypoint.Y_OFFSET = 100
	function TurretHealthbarWaypoint:init(...)
		TurretHealthbarWaypoint.super.init(self, ...)
		self:_add_bar("shield", Color(0.6, 0.6, 0.6))
	end
	
	function TurretHealthbarWaypoint:damage_received()
		TurretHealthbarWaypoint.super.damage_received(self)
		self._bars.shield:set_w(self._bar_width * self._unit:character_damage():shield_health_ratio())
	end
	
end
