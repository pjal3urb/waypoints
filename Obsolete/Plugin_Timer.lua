local init_original = HUDManager.init

function HUDManager:init(...)
	init_original(self, ...)
	
	if managers.gameinfo then
		managers.gameinfo:register_listener("timer_waypoint_listener", "timer", "set_active", callback(nil, _G, "custom_waypoint_timer_clbk"))
		managers.gameinfo:register_listener("timer_waypoint_listener", "timer", "update", callback(nil, _G, "custom_waypoint_timer_clbk"))
	end
end

function custom_waypoint_timer_clbk(event, key, data)
	local id = "timer_wp_" .. key

	if event == "set_active" then
		if data.active then
			local icon_table = {
				drill = "pd2_drill",
				hack = "pd2_computer",
				saw = "wp_saw",
				timer = "pd2_computer",
				securitylock = "pd2_computer",
				digital = "pd2_computer",
			}
			local params = {
				unit = data.unit,
				on_minimap = true,
				icon = { show = true, std_wp = icon_table[data.device_type or "timer"] },
				timer = { show = true, initial_value = 0 },
			}
			
			if data.id == 132864 then --Meltdown timer
				MeltdownTemperatureWaypoint = class(CustomWaypoint)
				
				MeltdownTemperatureWaypoint.update_timer = function(self, value, t, dt)
					self._settings.timer.value = value
					if self._settings.timer.show then
						self._components.timer:set_text(string.format("%d/50", math.floor(value)))
					end
				end
			
				managers.waypoints:add_waypoint(id, MeltdownTemperatureWaypoint, params)
			else
				managers.waypoints:add_waypoint(id, CustomWaypoint, params)
			end
		else
			managers.waypoints:remove_waypoint(id)
		end
	elseif event == "update" then
		managers.waypoints:set_waypoint_timer(id, data.timer_value)
	end
end

MeltdownTemperatureWaypoint = MeltdownTemperatureWaypoint or class()