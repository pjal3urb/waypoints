local init_original = HUDManager.init

function HUDManager:init(...)
	init_original(self, ...)
	
	if managers.gameinfo then
		for _, t in pairs({ "ammo_bag", "doc_bag", "body_bad", "grenade_crate" }) do
			managers.gameinfo:register_listener(t .. "_waypoint_listener", t, "set_active", callback(nil, _G, "custom_waypoint_bag_clbk", t))
			managers.gameinfo:register_listener(t .. "_waypoint_listener", t, "set_amount", callback(nil, _G, "custom_waypoint_bag_clbk", t))
			managers.gameinfo:register_listener(t .. "_waypoint_listener", t, "set_amount_offset", callback(nil, _G, "custom_waypoint_bag_clbk", t))
		end
		managers.gameinfo:register_listener("sentry_waypoint_listener", "sentry", "set_active", callback(nil, _G, "custom_waypoint_sentry_clbk"))
		managers.gameinfo:register_listener("sentry_waypoint_listener", "sentry", "set_ammo_ratio", callback(nil, _G, "custom_waypoint_sentry_clbk"))
	end
end

local function add_waypoint(id, unit, position, texture, texture_rect, text, color, visible_angle, visible_distance)
	local params = {
		unit = unit,
		position = position,
		color = color,
		visible_angle = visible_angle,
		visible_distance = visible_distance or { max = 1000 },
		scale = 1.25,
		icon = { show = true, texture = texture, texture_rect = texture_rect },
		label = { show = true, text = text },
		component_order = { "icon", "label", "distance", "timer", "duration" },
	}
	managers.waypoints:add_waypoint(id, CustomWaypoint, params)
end

function custom_waypoint_bag_clbk(type, event, key, data)
	if data.aggregate_members then return end
	
	local id = "bag_wp_" .. key
	
	if event == "set_active" then
		if data.active then
			local icon_map = {
				ammo_bag = { texture = "guis/textures/pd2/skilltree/icons_atlas", texture_rect = { 1*64, 0, 64, 64 } },
				doc_bag = { texture = "guis/textures/pd2/skilltree/icons_atlas", texture_rect = { 2*64, 7*64, 64, 64 } },
				body_bag = { texture = "guis/textures/pd2/skilltree/icons_atlas", texture_rect = { 5*64, 11*64, 64, 64 } },
				grenade_crate = { texture = "guis/dlcs/big_bank/textures/pd2/pre_planning/preplan_icon_types", texture_rect = { 1*48, 0, 48, 48 } },
			}
			
			local amount = (data.amount or 0) + (data.amount_offset or 0)
			if type == "ammo_bag" then
				amount = amount * 100
			end
			add_waypoint(id, data.unit, data.position, icon_map[type].texture, icon_map[type].texture_rect, tostring(amount))
		else
			managers.waypoints:remove_waypoint(id)
		end
	elseif event == "set_amount" or event == "set_amount_offset" then
		local amount = (data.amount or 0) + (data.amount_offset or 0)
		if type == "ammo_bag" then
			amount = amount * 100
		end
		managers.waypoints:set_waypoint_label(id, math.round(amount))
	end
end

function custom_waypoint_sentry_clbk(event, key, data)
	local id = "sentry_wp_" .. key
	
	if event == "set_active" then
		if data.active then
			local text = string.format("%.0f%%", (data.ammo_ratio or 0) * 100)
			add_waypoint(id, data.unit, data.position, "guis/textures/pd2/skilltree/icons_atlas", {7*64, 5*64, 64, 64}, text, Color.red, { max = 12.5 }, { max = 2000 })
		else
			managers.waypoints:remove_waypoint(id)
		end
	elseif event == "set_ammo_ratio" then
		managers.waypoints:set_waypoint_label(id, string.format("%.0f%%", (data.ammo_ratio or 0) * 100))
	end
end
