If adding new custom waypoints for use with the manager, it's highly recommended that a subclass of CustomWaypoint is used. It provides basic functionality for waypoints,
including on-screen and off-screen positioning as well as enabling/disabling waypoints dynamically. The class is loaded as a *pre-require* to "lib/setups/gamesetup", so
any subclasses can be created either as a post-require to the same library script, or any other that is loaded afterwards.

Managing waypoints is done via the manager:
	managers.waypoints:add_waypoint(id, class,data)
		Adds a new waypoint. If the ID provided is not unique, no new waypoint is created. The waypoint superclass must be provided with a 3D vector representing the
		world position of the waypoint as part of its extra parameters table, index by the value "position". It will also accept the optional "show_offscreen" boolean
		value to determine if the waypoint should be shown if projected outside the viewable screen space. Subclasses may opt to represent the world position in other 
		ways and implement additional parameters.
			id: A unique string identifier for the waypoint
			class: The class to use for creating the waypoint, either as a raw class table or the global class name as a string
			data: Table of extra parameters to forward to the class init function
		
	managers.waypoints:remove_waypoint(id)
		Removes a waypoint
			id: The string identifier of the waypoint
	
	managers.waypoint:set_waypoint_attribute(id, attribute, ...)
		Sends an event to a specified waypoint with optional data. Requires that the waypoint has a function implemented with the name "set_<ATTR>"
			id: The string identifier of the waypoint
			attribute: The string identifier of the event. Used to determine the function to call by prepending "set_"
			...: additional, arbitrary number of arguments to forward to the waypoint event function


Fine-tuning the behavior and layout can be done by overriding a set of pre-defined functons in the waypoint:
	CustomWaypoint:_alive()
		Returns true if the waypoint is still active. If it returns true, the waypoint will be immediately removed.

	CustomWaypoint:_update_world_position(t, dt)
		Returns a Vector3 object representing the world position of the waypoint. In superclass implementation, returns the value of the member variable _position
			t: update timestamp
			dt: time elapsed since last update
			
	CustomWaypoint:_displace_screen_position()
		Returns x, y coordinates to displace the waypoint screen coordinates (useful for moving the waypoint up/down a static amount on the screen)
			
	CustomWaypoint:_check_onscreen_visibility(distance, dot, angle)
		Called when the waypoint is projected onto the screenspace and should return either true or false depending on whether the waypoint should be visible or not.
		In superclass implementation, returns true (always show on-screen waypoints)
			distance: Distance from the camera position to the world position of the 
			dot: Dot product between view forward and waypoint vectors
			angle: Angle (in degrees) between view forward and waypoint vectors
			
	CustomWaypoint:_check_offscreen_visibility(distance, dot, angle)
		Called when the waypoint is projected outside the screenspace and should return either true or false depending on whether the waypoint should be visible or
		not. In superclass implementation, returns the value of the member variable _show_offscreen.
			distance: Distance from the camera position to the world position of the 
			dot: Dot product between view forward and waypoint vectors
			angle: Angle (in degrees) between view forward and waypoint vectors
			
	CustomWaypoint:_update(t, dt, distance, dot, angle, x, y)
		Called every update cycle provided that the waypoint is currently enabled, whether visible or not
			t: update timestamp
			dt: time elapsed since last update
			distance: Distance from the camera position to the world position of the 
			dot: Dot product between view forward and waypoint vectors
			angle: Angle (in degrees) between view forward and waypoint vectors
			x, y: Screen x and y coordinates of the waypoint

	CustomWaypoint:_update_disabled(t, dt)
		Called every update cycle provided that the waypoint is currently disabled
			t: update timestamp
			dt: time elapsed since last update
			distance: Distance from the camera position to the world position of the 
			dot: Dot product between view forward and waypoint vectors
			angle: Angle (in degrees) between view forward and waypoint vectors
			x, y: Screen x and y coordinates of the waypoint

	CustomWaypoint:_onscreen_state_change()
		Called when a waypoint move on- or off-screen. The current on-screen status is stored in the member variable _onscreen
		
	CustomWaypoint:_visibility_state_change()
		Called when a waypoint changes its visibility state. The current state is stored in the member variable _visible
		
Other functions to override are:
	CustomWaypoint:init(id, ws, data)
		The initialization function. Highly recommended that the superclass init function is called first with "CustomWaypoint.init(self, id, ws, data)" to
		create the waypoint panel and setup initial settings.
			id: The unique string identifier for the waypoint
			ws: The screen workspace on which the waypoint panel resides
			data: Table of extra parameters for the waypoint
			
	CustomWaypoint:post_init()
		Called once immediately after the waypoint has been initialized.
			
	CustomWaypoint:destroy()
		Called once immediately before the waypoint is deleted by the manager.
		
In addition, two member variables for the behavior of the off-screen arrow may be relevant to change if the layout of the waypoint is adjusted dynamically:
	_arrow_radius:
		A numeric value for the radius of the circle the off-screen arrow will move along. Defaults to the max dimension of the panel after the init function
		has completed.
	_arrow_center
		An array with 2 values reprensenting the x and y position on the waypoint panel the off-screen arrow should be centered around. Defaults to the center
		position of the panel after the init function has completed.


To get back to the main portal repository, [click me!](https://bitbucket.org/pjal3urb/pd2-mods)