local last_damage_t = 0

local function check_damage_popup(unit, damage)
	local t = Application:time()
	
	if last_damage_t + DamageDealtWaypoint.THRESHOLD < t then
		last_damage_t = t
	end
	
	local id = string.format("damage_wp_%s_%f", tostring(unit:key()), last_damage_t)
	local wp = managers.waypoints:get_waypoint(id)
	
	if not wp then
		local position = unit:in_slot(25) and unit:position() or unit:movement():m_head_pos()
		wp = managers.waypoints:add_waypoint(id, DamageDealtWaypoint, { position = position + math.UP * 50 })
	end
	
	wp:add_damage(damage)
end

local on_damage_dealt_original = PlayerManager.on_damage_dealt

function PlayerManager:on_damage_dealt(unit, damage_info, ...)
	check_damage_popup(unit, damage_info.damage)
	return on_damage_dealt_original(self, unit, damage_info, ...)
end




if not CustomWaypoint then return end	--Suppress error in menu


DamageDealtWaypoint = DamageDealtWaypoint or class(CustomWaypoint)

DamageDealtWaypoint.SCALE = 1
DamageDealtWaypoint.THRESHOLD = 0.35
DamageDealtWaypoint.TTL = 0.75
DamageDealtWaypoint.MAX_OFFSET = 50

function DamageDealtWaypoint:init(id, ws, data)
	DamageDealtWaypoint.super.init(self, id, ws, data)
	
	self._panel:set_size(50 * DamageDealtWaypoint.SCALE, 25 * DamageDealtWaypoint.SCALE)
	self._panel:set_layer(10)
	
	self._text = self._panel:text({
		name = "text",
		font = tweak_data.hud.medium_font_noshadow,
		font_size = self._panel:h() * 0.95,
		align = "center",
		vertical = "center",
		w = self._panel:w(),
		h = self._panel:h(),
	})
	
	self._damage = 0
	self._t = 0
end

function DamageDealtWaypoint:_alive()
	return self._enabled  and self._t < DamageDealtWaypoint.TTL
end

function DamageDealtWaypoint:_displace_screen_position()
	return 0, -self._t/DamageDealtWaypoint.TTL * DamageDealtWaypoint.MAX_OFFSET
end

function DamageDealtWaypoint:_update(t, dt, distance, dot, angle, x, y)
	self._t = self._t + dt
end

function DamageDealtWaypoint:add_damage(damage)
	self._damage = self._damage + (damage or 0)
	self._text:set_text(string.format("%.0f", self._damage * 10))
end
